var elixir = require('laravel-elixir');
require('./elixir-extensions');

elixir(function (mix) {

    mix
        .sass('main.scss')
        .sass('admin.scss')
        .babel([
            'facebookUtils.js',
            'main.js'
        ], 'public/js/main.js')
        .babel('admin.js', 'public/js/admin.js')
        .scripts([
            // bower:js
            // endbower
            'bower_components/gentelella/vendors/jquery/dist/jquery.js',
            'bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.js',
            'bower_components/gentelella/build/js/custom.js'
        ], 'public/js/vendor-front.js', 'bower_components')
        .scripts([
            // bower:js
            // endbower
            'bower_components/gentelella/vendors/jquery/dist/jquery.js',
            'bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.js',
            'bower_components/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js',
            'bower_components/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
            'bower_components/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
            'bower_components/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
            'bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js',
            'bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js',
            'bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js',
            'bower_components/gentelella/vendors/pdfmake/build/pdfmake.min.js',
            'bower_components/gentelella/vendors/pdfmake/build/vfs_fonts.js',
            'bower_components/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
            'bower_components/gentelella/build/js/custom.js'
        ], 'public/js/vendor-admin.js', 'bower_components')
        .styles([
            // bower:css
            // endbower
            'bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.css',
            'bower_components/gentelella/vendors/font-awesome/css/font-awesome.css',
            'bower_components/gentelella/build/css/custom.css'
        ], 'public/css/vendor-front.css', 'bower_components')
        .styles([
            // bower:css
            // endbower
            'bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.css',
            'bower_components/gentelella/vendors/font-awesome/css/font-awesome.css',
            'bower_components/gentelella/build/css/custom.css'
        ], 'public/css/vendor-admin.css', 'bower_components')

        /**************/
        /* Copy Fonts */
        /**************/

        // Bootstrap
        .copy('bower_components/gentelella/vendors/bootstrap/fonts/', 'public/fonts')
        // Font awesome
        .copy('bower_components/gentelella/vendors/font-awesome/fonts/', 'public/fonts')

        .images()
        .wiredep()
        .minifyCss()
        .compress()
        .browserSync({
            proxy: process.env.APP_URL
        });


});

<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder{

    public function run(){

        DB::table('users')->delete();
        User::create([
            'name' => 'AdminBox',
            'email' => 'admin@box.agency',
            'password' => bcrypt('admin@box.2016')
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder{

    public function run(){
        DB::table('users')->delete();
        User::create([
            'full_name' => 'box agency',
            'username' => 'admin',
            'email' => 'admin@box.agency',
            'password' => bcrypt('admin@2016')
        ]);
    }
}

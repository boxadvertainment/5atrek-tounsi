<?php

use Illuminate\Database\Seeder;
use App\Participant;

class ParticipantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Participant::create([
            "facebook_id"=> "110282892738616",
            "participant_nom"=> "Mia",
            "participant_prenom"=> "Spears",
            "participant_email"=> "miaspears@assistia.com",
            "participant_gender"=> "female",
            "participant_ip"=> "71.13.36.112"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed192a281eb46b60e8ad",
            "participant_nom"=> "Key",
            "participant_prenom"=> "Shaffer",
            "participant_email"=> "keyshaffer@assistia.com",
            "participant_gender"=> "male",
            "participant_ip"=> "23.201.245.129"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed19b01957613f7e8167",
            "participant_nom"=> "Rosario",
            "participant_prenom"=> "Cooley",
            "participant_email"=> "rosariocooley@assistia.com",
            "participant_gender"=> "male",
            "participant_ip"=> "77.207.25.148"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed1999612c6fa7f97547",
            "participant_nom"=> "Terry",
            "participant_prenom"=> "Steele",
            "participant_email"=> "terrysteele@assistia.com",
            "participant_gender"=> "male",
            "participant_ip"=> "2.10.171.137"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed197d2b49f243a6e894",
            "participant_nom"=> "Joyce",
            "participant_prenom"=> "Ortiz",
            "participant_email"=> "joyceortiz@assistia.com",
            "participant_gender"=> "female",
            "participant_ip"=> "103.83.185.187"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed19ba56ceae1a74bae1",
            "participant_nom"=> "Liz",
            "participant_prenom"=> "Wolfe",
            "participant_email"=> "lizwolfe@assistia.com",
            "participant_gender"=> "female",
            "participant_ip"=> "102.228.144.183"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed196268de7ab87e7370",
            "participant_nom"=> "Sybil",
            "participant_prenom"=> "Maxwell",
            "participant_email"=> "sybilmaxwell@assistia.com",
            "participant_gender"=> "female",
            "participant_ip"=> "114.77.228.79"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed19320ba1b0f3e02d6a",
            "participant_nom"=> "Leona",
            "participant_prenom"=> "Raymond",
            "participant_email"=> "leonaraymond@assistia.com",
            "participant_gender"=> "female",
            "participant_ip"=> "6.127.254.162"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed19fe8f464db7049657",
            "participant_nom"=> "Reilly",
            "participant_prenom"=> "Cox",
            "participant_email"=> "reillycox@assistia.com",
            "participant_gender"=> "male",
            "participant_ip"=> "152.242.169.118"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed19a4f09d0843d51b47",
            "participant_nom"=> "Christian",
            "participant_prenom"=> "Mason",
            "participant_email"=> "christianmason@assistia.com",
            "participant_gender"=> "female",
            "participant_ip"=> "186.163.161.70"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed19af3a86cfcc761043",
            "participant_nom"=> "Monique",
            "participant_prenom"=> "Cook",
            "participant_email"=> "moniquecook@assistia.com",
            "participant_gender"=> "female",
            "participant_ip"=> "43.181.32.92"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed1967c17b3e5664351f",
            "participant_nom"=> "French",
            "participant_prenom"=> "Mcconnell",
            "participant_email"=> "frenchmcconnell@assistia.com",
            "participant_gender"=> "male",
            "participant_ip"=> "149.29.109.155"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed19693d410ddcc7480a",
            "participant_nom"=> "Hamilton",
            "participant_prenom"=> "Blankenship",
            "participant_email"=> "hamiltonblankenship@assistia.com",
            "participant_gender"=> "male",
            "participant_ip"=> "225.179.44.89"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed1935a8654306b5aa2c",
            "participant_nom"=> "Lynn",
            "participant_prenom"=> "Norris",
            "participant_email"=> "lynnnorris@assistia.com",
            "participant_gender"=> "male",
            "participant_ip"=> "227.196.144.16"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed199f01895ef32352da",
            "participant_nom"=> "May",
            "participant_prenom"=> "Franco",
            "participant_email"=> "mayfranco@assistia.com",
            "participant_gender"=> "female",
            "participant_ip"=> "193.126.151.255"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed19d6d02c9ece611972",
            "participant_nom"=> "Marilyn",
            "participant_prenom"=> "Humphrey",
            "participant_email"=> "marilynhumphrey@assistia.com",
            "participant_gender"=> "female",
            "participant_ip"=> "233.106.28.135"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed19279b08ece771f650",
            "participant_nom"=> "Dianne",
            "participant_prenom"=> "Bryant",
            "participant_email"=> "diannebryant@assistia.com",
            "participant_gender"=> "female",
            "participant_ip"=> "235.2.180.122"
        ]);
        Participant::create([
            "facebook_id"=> "5763ed19ed404f112b623eb3",
            "participant_nom"=> "Morgan",
            "participant_prenom"=> "Frazier",
            "participant_email"=> "morganfrazier@assistia.com",
            "participant_gender"=> "male",
            "participant_ip"=> "6.123.23.5"
        ]);

    }
}

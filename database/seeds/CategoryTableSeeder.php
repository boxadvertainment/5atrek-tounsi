<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'categorie_name' => 'Social'
        ]);

        Category::create([
            'categorie_name' => 'Musique'
        ]);

        Category::create([
            'categorie_name' => 'Education'
        ]);

        Category::create([
            'categorie_name' => 'Santé'
        ]);

        Category::create([
            'categorie_name' => 'Art'
        ]);

        Category::create([
            'categorie_name' => 'Innovation'
        ]);

        Category::create([
            'categorie_name' => 'Autres'
        ]);

    }
}

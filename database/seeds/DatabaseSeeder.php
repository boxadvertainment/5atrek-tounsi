<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder{

    public function run(){
        $this->call('UsersTableSeeder');
        $this->call('ParticipantTableSeeder');
        $this->call('CategoryTableSeeder');
        $this->call('PostTableSeeder');
        $this->call('YoutubeTableSeeder');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table){
            $table->increments('id');
            $table->string('facebook_id')->unique();
            $table->string('participant_nom');
            $table->string('participant_prenom');
            $table->string('participant_email');
            $table->string('participant_gender');
            $table->ipAddress('participant_ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

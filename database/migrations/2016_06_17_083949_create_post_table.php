<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('participant_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->string('post_titre');
            $table->text('post_text');
            $table->string('post_date');
            $table->string('post_region');
            $table->enum('post_media_type', ['image', 'video']);
            $table->string('post_media_link');
            $table->ipAddress('post_ip');
            $table->enum('post_status', ['pending','accept','refused']);
            $table->timestamps();

            $table->foreign('participant_id','post_participant_fk')
                ->references('id')
                ->on('participants')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('category_id', 'post_category_fk')
                ->references('id')
                ->on('categories')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}

<?php

return [
    'format' => [
        'video' => [
            'video/3gpp' => '3gp',
            'video/jpeg' => 'jpgv',
            'video/mp4' => 'mp4',
            'video/mpeg' => 'mpeg',
            'video/quicktime' => 'qt',
            'video/webm' => 'webm',
            'video/x-flv' => 'flv',
            'video/x-ms-wmv' => 'wmv',
            'video/x-msvideo' => 'avi',
        ],
        'image' => [
            'image/png' => 'png',
            'image/gif' => 'gif',
            'image/jpeg' => 'jpeg',
            'image/bmp' => 'bmp'
        ],
    ],
];

/*return ['video/3gpp' => '3gp',
	'video/jpeg' => 'jpgv',
	'video/mp4' => 'mp4',
	'video/mpeg' => 'mpeg',
	'video/quicktime' => 'qt',
	'video/webm' => 'webm',
	'video/x-flv' => 'flv',
	'video/x-ms-wmv' => 'wmv',
	'video/x-msvideo' => 'avi',
	'image/png' => 'png',
	'image/gif' => 'gif',
	'image/jpeg' => 'jpeg',
	'image/bmp' => 'bmp'];*/
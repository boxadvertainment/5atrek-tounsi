<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class Category extends Model
{
    //

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function getNumber(){
        return Post::where('category_id', $this->id)->count();
    }

    public function getPourcentage(){
        $total = Post::all()->count();
        $pourcentage = ($this->getNumber() / $total) * 100;
        return round($pourcentage);
    }
}

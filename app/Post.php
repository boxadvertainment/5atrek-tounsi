<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function participant()
    {
        return $this->belongsTo('App\Participant');
    }


    public function getNumberByStatus($status)
    {
        return Post::where('post_status', $status)->count();
    }

    public function getListByStatus($status)
    {
        return Post::where('post_status', $status)->get();
    }

    public function getMessageImageThumb(){
        switch($this->post_media_type){
            case 'video':
                return sprintf('https://img.youtube.com/vi/%s/0.jpg',
                    $this->post_media_link
                );
                break;

            case 'image':
                return '/app/images/thumb/'.$this->post_media_link;
                //return url('app/images/255/' . $this->post_media_link);
                break;
        }
    }
    public function getMessageImageOptim(){
        switch($this->post_media_type){
            case 'video':
                return sprintf('https://img.youtube.com/vi/%s/0.jpg',
                    $this->post_media_link
                );
                break;

            case 'image':
                return '/app/images/optimised/'.$this->post_media_link;
                //return url('app/images/255/' . $this->post_media_link);
                break;
        }
    }
}

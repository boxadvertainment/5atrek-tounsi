<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //protected $table = 'posts';

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function participant()
    {
        return $this->belongsTo('App\Participant');
    }


    public function getNumberByStatus($status)
    {
        return Post::where('post_status', $status)->count();
    }

    public function getListByStatus($status)
    {
        return Post::where('post_status', $status)->get();
    }
}

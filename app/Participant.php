<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    public function post()
    {
        return $this->hasOne('App\Post');
    }

    public function getFullName()
    {
        return $this->participant_nom . " " . $this->participant_prenom;
    }
}

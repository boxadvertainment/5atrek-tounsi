<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;
use App\Http\Requests;
use App\Category;
use App\Post;

class AppController extends Controller{

    public function index(){
        return view('index')->with([
            'list' => Category::all()
        ]);
    }
    
    public function formPost(){
        $list = Category::all();
        return view('post')->with([
            'list' => $list
        ]);
    }

    public function viewPost($name){
        if($name == "all"){
            $list = Post::all();
            $title = 'Liste des posts';
        }else{
            $cat = Category::select('categorie_name')->where('id', $name)->first();
            $list= Post::where('category_id', $name)->get();
            $title = 'Liste des '.$cat->categorie_name;
        }
        return view('affiche-post')->with([
            'list' => $list,
            'title_page' => $title
        ]);
    }

    public function postCreate(){
        if(
            ($post_titre= \Request::input('post_titre')) == false ||
            ($post_text= \Request::input('post_text')) == false ||
            ($post_media_link = \Request::file('post_media_link')) == false ||
            ($post_category = \Request::input('category_id')) == false ||
            ($post_date = \Request::input('post_date')) == false ||
            ($post_region = \Request::input('post_region')) == false
        ){
            return \Response::json(
                [
                    'notification' => [
                        'type' => 'error',
                        'message' => "Tous les champs sont obligatoires."
                    ]
                ],
                400
            );
        }

        $uploadConf = config('upload');
        $fileExtention = $post_media_link->guessExtension();

        $post = new Post;
        $post->post_titre = $post_titre;
        $post->category_id = $post_category;
        $post->post_text = $post_text;
        $post->post_date = $post_date;
        $post->post_region = $post_region;
        $post->post_ip = \Request::ip();
        $post->post_status = "pending";

        if(
        in_array(
            strtolower($fileExtention),
            array_merge(
                $uploadConf['format']['video'],
                $uploadConf['format']['image']
            )
        )
        ) {

            $mediaFieName = Carbon::now()->timestamp . '.' . $fileExtention;

            if (in_array(strtolower($fileExtention), $uploadConf['format']['video'])) {
                $post_media_link->move(
                    storage_path('app/videos/'),
                    $mediaFieName
                );
                
                if(
                    (
                    $youtubeVideoId = \Youtube::upload(
                        'videos/'.$mediaFieName,
                        [
                            'title' => "Un message pour : Moi meme",
                            'description' => $post->post_text,
                            'tags' => [
                                'zitouna',
                                'omra',
                                'Mohamed'
                            ]
                        ],
                        'unlisted'
                    )
                    ) != false
                ) {
                    $post->post_media_type = 'video';
                    $post->post_media_link = $youtubeVideoId;
                }
            } else {

                $dirOriginal = 'app/images/original/';
                $dirThumb = 'app/images/thumb/';
                $dirOptimised = 'app/images/optimised/';

                if (!\File::isDirectory($dirOriginal)) {
                    \File::makeDirectory($dirOriginal, 755, true);
                }
                if (!\File::isDirectory($dirThumb)) {
                    \File::makeDirectory($dirThumb, 755, true);
                }
                if (!\File::isDirectory($dirOptimised)) {
                    \File::makeDirectory($dirOptimised, 755, true);
                }

                $path1 = $dirThumb . $mediaFieName;
                Image::make($post_media_link)->fit(255, 153)->save($path1);

                $path1 = $dirOptimised . $mediaFieName;
                Image::make($post_media_link)->resize(600, null)->save($path1);

                $post_media_link->move($dirOriginal, $mediaFieName);


                $post->post_media_type = 'image';
                $post->post_media_link = $mediaFieName;

            }
            $post->participant_id = 19;
            //app('user')->message()->save($message);
            $post->save();

            return redirect()->route('index');
        }else{
            return \Response::json(
                [
                    'notification' => [
                        'type' => 'error',
                        'message' => "Format du fichier non supporté :( ". strtoupper($fileExtention)
                    ]
                ],
                400
            );
        }
    }
}

<?php

namespace App\Http\Controllers;

use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use App\Http\Requests;
use App\Category;
use App\Post;
use App\Participant;

class AppController extends Controller{

    public function youtubeAuth (){}

    public function index($category = 'all'){

        if($category == "all" || $category == ""){
            $list = Post::where('post_status', 'accept')->orderBy('updated_at','desc')->paginate(9);
            $title = 'Liste de toutes les actions';
        }else{
            $cat = Category::select('categorie_name')->where('id', $category)->first();
            $list= Post::where('category_id', $category)->where('post_status', 'accept')->orderBy('updated_at', 'desc')->paginate(9);
            $title = 'Liste des actions '.$cat->categorie_name;
        }
        if (Request::ajax()) {
            return \Response::json(\View::make('posts', array('list' => $list))->render());
        }else{
            return view('index')->with([
                'category_list'     => Category::all(),
                'list'              => $list,
                'page_title'        => $title,
                'actions_count'      => count($list),
                'current_category'  => $category
            ]);
        }
    }

    public function postsList($category){
        if($category == "all"){
            $list = Post::all();
            $title = 'Liste des posts';
        }else{
            $cat = Category::select('categorie_name')->where('id', $category)->first();
            $list= Post::where('category_id', $category)->get();
            $title = 'Liste des '.$cat->categorie_name;
        }
        return view('affiche-post')->with([
            'category_list' => Category::all(),
            'list' => $list,
            'title_page' => $title
        ]);
    }

    public function postDetail($id){
        $post= Post::where('id', $id)->first();
        return view('post_detail')->with([
            'post' => $post
        ]);
    }

    public function postCreate(){
        //dd(\Request::all());
        if(
            ($post_titre            = \Request::input('post_titre')) == false ||
            ($post_text             = \Request::input('post_text')) == false ||
            ($post_media_link       = \Request::file('post_media_link')) == false ||
            ($post_category         = \Request::input('category_id')) == false ||
            ($post_region           = \Request::input('post_region')) == false ||
            ($post_user_first_name  = \Request::input('user-first-name')) == false ||
            ($post_user_last_name   = \Request::input('user-last-name')) == false ||
            ($post_user_email       = \Request::input('user-email')) == false ||
            ($post_user_fb_id       = \Request::input('user-fb-id')) == false
        ){
            return \Response::json([
                'success'   => false,
                'type'      => 'warning',
                'message'   => "Tous les champs sont obligatoires."
            ], 200);
        }

        $uploadConf = config('upload');
        $fileExtention = $post_media_link->guessExtension();


        $user = \DB::table('participants')
            ->where('facebook_id', '=', $post_user_fb_id)
            ->first();

        if( count($user) == 0 ){

            $user = new Participant;
            $user->facebook_id          = $post_user_fb_id;
            $user->participant_nom      = $post_user_first_name;
            $user->participant_prenom   = $post_user_last_name;
            $user->participant_email    = $post_user_email;
            $user->participant_ip       = \Request::ip();

            if(!$user->save()) {
                return \Response::json([
                    'success'   => false,
                    'type'      => 'warning',
                    'message'   => "Un problème est survenu lors de l'enregistrement de votre post. Veuillez réessayer. Merci"
                ], 200);
            }
        }

        $post = new Post;
        $post->post_titre = $post_titre;
        $post->participant_id = $user->id;
        $post->category_id = $post_category;
        $post->post_text = $post_text;
        $post->post_date = \Request::input('post_date');
        $post->post_region = $post_region;
        $post->post_ip = \Request::ip();
        $post->post_status = "pending";

        if(
        in_array(
            strtolower($fileExtention),
            array_merge(
                $uploadConf['format']['video'],
                $uploadConf['format']['image']
            )
        )
        ) {

            $mediaFieName = $user->id.'_'.Carbon::now()->timestamp . '.' . $fileExtention;

            $dirVideo = storage_path('app/videos');
            if (!\File::isDirectory($dirVideo)) {
                \File::makeDirectory($dirVideo, 755, true);
            }

            if (in_array(strtolower($fileExtention), $uploadConf['format']['video'])) {
                $post_media_link->move(
                    storage_path('app/videos/'),
                    $mediaFieName
                );
                
                if(
                    (
                    $youtubeVideoId = \Youtube::upload(
                        'videos/'.$mediaFieName,
                        [
                            'title'         => $post_titre,
                            'description'   => $post->post_text,
                            'tags' => [
                                '5atrek Tounsi',
                                'Tunisie'
                            ]
                        ],
                        'unlisted'
                    )
                    ) != false
                ) {
                    $post->post_media_type = 'video';
                    $post->post_media_link = $youtubeVideoId;
                }
            } else {

                $dirOriginal = 'app/images/original/';
                $dirThumb = 'app/images/thumb/';
                $dirOptimised = 'app/images/optimised/';

                if (!\File::isDirectory($dirOriginal)) {
                    \File::makeDirectory($dirOriginal, 755, true);
                }
                if (!\File::isDirectory($dirThumb)) {
                    \File::makeDirectory($dirThumb, 755, true);
                }
                if (!\File::isDirectory($dirOptimised)) {
                    \File::makeDirectory($dirOptimised, 755, true);
                }

                $path1 = $dirThumb . $mediaFieName;
                Image::make($post_media_link)->fit(375, 225)->save($path1);

                $path1 = $dirOptimised . $mediaFieName;
                Image::make($post_media_link)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path1);

                $post_media_link->move($dirOriginal, $mediaFieName);


                $post->post_media_type = 'image';
                $post->post_media_link = $mediaFieName;

            }

            if(!$post->save()) {
                return \Response::json([
                    'success'   => false,
                    'type'      => 'warning',
                    'message'   => "Un problème est survenu lors de l'enregistrement de votre post. Veuillez réessayer. Merci"
                ], 200);
            }

            return \Response::json([
                'success'   => true,
                'type'      => 'success',
                'message'   => "Merci pour votre participation, Votre publication sera public aprés modération."
            ], 200);

        }else{

            return \Response::json([
                'success'   => false,
                'type'      => 'error',
                'message'   => "Format du fichier non supporté :( ". strtoupper($fileExtention)
            ], 200);

        }
    }
}

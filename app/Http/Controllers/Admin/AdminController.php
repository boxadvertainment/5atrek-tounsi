<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use App\Post;
use App\Participant;
use App\Category;

class AdminController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function post_accept($id){
        $post = Post::find($id);
        $post->post_status = "accept";
        $post->save();
        return redirect()->route('viewPosts');
    }

    public function post_refused($id){
        $post = Post::find($id);
        $post->post_status = "refused";
        $post->save();
        return redirect()->route('viewPosts');
    }
    
    public function index(){
        $p = new Participant();
        $nbParticipant = Participant::all()->count();
        $nbPost = Post::all()->count();
        $postDate = Post::select(\DB::raw('SUBSTR(post_date, 1, 10) as date'))
            ->orderBy('date')
            ->get()
            ->groupBy('date');
        $participantMonth = Participant::select(\DB::raw('SUBSTR(created_at, 6, 2) as monthe'))
                                        ->orderBy('monthe')
                                        ->get()
                                        ->groupBy('monthe');
        $postMonth = Post::select(\DB::raw('SUBSTR(created_at, 6, 2) as monthe'))
                                        ->orderBy('monthe')
                                        ->get()
                                        ->groupBy('monthe');
        $postCategory = Post::all()->groupBy('category_id');

        return view('admin.index')->with([
            'nbParticipant' => $nbParticipant,
            'nbPost' => $nbPost,
            'postDate' => $postDate,
            'participantMonth' => $participantMonth,
            'postMonth' => $postMonth,
            'postCategory' => $postCategory
        ]);
    }
    
    public function addCategory(){
        $cat = new Category;
        $cat->categorie_name = \Request::input('categorie_name');
        $cat->save();
        return redirect()->route('viewCategories');
    }

    public function postByCategories($id){
        $cat = Category::find($id);
        return view('admin.postByCategories')->with([
            'category' => $cat->categorie_name,
            'list' =>Post::where('category_id', $id)->get()
        ]);
    }

    public function formCategory(){
        return view('admin.add_category');
    }
    
    public function categoryDetail($id){
        return view('admin.category_detail')->with([
            'category' => Category::find($id)
        ]);
    }

    public function categories(){
        $list = Category::all();
        return view('admin.category')->with([
            'list' => $list,
            'total' => $list->count()
        ]);
    }

    public function participants(){
        return view('admin.participant')->with([
            'list' => Participant::all()
        ]);
    }

    public function editParticipant($id, Request $request){
        $participant = Participant::find($id);
        return view('admin.editParticipant')->with([
            'participant' => $participant
        ]);
    }

    public function updateParticipant($id, Request $request){


        $validator = \Validator::make($request->all(), [
            'participant_nom'  =>  'required',
            'participant_email' =>  'required|email',
            'participant_prenom' =>  'required',
            'facebook_id'  =>  'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => implode('<br>', $validator->errors()->all())]);
        }else{
            Participant::where('id', $id)
                ->update([
                    'participant_nom' => $request->participant_nom,
                    'participant_prenom' => $request->participant_prenom,
                    'facebook_id' => $request->facebook_id,
                    'participant_email' => $request->participant_email
                ]);
            return view('admin.participant')->with([
                'list' => Participant::all()
            ]);
        }
    }

    public function posts(){
        $p = new Post();
        return view('admin.posts')->with([
            'listpending' => $p->getListByStatus("pending"),
            'listaccept' => $p->getListByStatus("accept"),
            'listrefused' => $p->getListByStatus("refused"),
            'nbPending' => $p->getNumberByStatus("pending"),
            'nbAccepted' => $p->getNumberByStatus("accept"),
            'nbRefused' => $p->getNumberByStatus("refused")
        ]);
    }

    public function postDetail($id){
        $post = Post::find($id);
        return view('admin.post_detail')->with(['post' => $post,'category_list' => Category::all() ]);
    }

    public function editPost(){
        $post = Post::find(\Request::input('id'));
        $post_media_link = \Request::file('post_media_link');

        if(
            ($post_titre= \Request::input('post_titre')) == false ||
            ($post_text= \Request::input('post_text')) == false ||
            ($post_category = \Request::input('category_id')) == false ||
            ($post_date = \Request::input('post_date')) == false ||
            ($post_region = \Request::input('post_region')) == false ||
            ($post_status = \Request::input('post_status')) == false
        ){
            return \Response::json(
                [
                    'notification' => [
                        'type' => 'error',
                        'message' => "Tous les champs sont obligatoires."
                    ]
                ],
                400
            );
        }

        $post->post_titre = $post_titre;
        $post->post_text = $post_text;
        $post->category_id = $post_category;
        $post->post_date = $post_date;
        $post->post_region = $post_region;
        $post->post_status = $post_status;

        if(!empty($post_media_link)){
            $uploadConf = config('upload');
            $fileExtention = $post_media_link->guessExtension();

            if(
            in_array(
                strtolower($fileExtention),
                array_merge(
                    $uploadConf['format']['video'],
                    $uploadConf['format']['image']
                )
            )
            ) {
                $mediaFieName = Carbon::now()->timestamp . '.' . $fileExtention;

                if (in_array(strtolower($fileExtention), $uploadConf['format']['video'])) {
                    $post_media_link->move(
                        storage_path('app/videos/'),
                        $mediaFieName
                    );

                    if (
                        (
                        $youtubeVideoId = \Youtube::upload(
                            'videos/'.$mediaFieName,
                            [
                                'title' => "Un message pour : Makdoudi" /*. app('user')->user_full_name*/,
                                'description' => $post->post_text,
                                'tags' => [
                                    'Box',
                                    'Test',
                                    /*app('user')->user_full_name*/
                                    'Maohamed Makdoudi'
                                ]
                            ],
                            'unlisted'
                        )
                        ) != false
                    ) {
                        $message->post_media_type = 'video';
                        $message->post_media_link = $youtubeVideoId;
                    }
                } else {

                    $dirOriginal = 'app/images/original/';
                    $dirThumb = 'app/images/thumb/';
                    $dirOptimised = 'app/images/optimised/';

                    if (!\File::isDirectory($dirOriginal)) {
                        \File::makeDirectory($dirOriginal, 755, true);
                    }
                    if (!\File::isDirectory($dirThumb)) {
                        \File::makeDirectory($dirThumb, 755, true);
                    }
                    if (!\File::isDirectory($dirOptimised)) {
                        \File::makeDirectory($dirOptimised, 755, true);
                    }

                    $path1 = $dirThumb . $mediaFieName;
                    Image::make($post_media_link)->fit(255, 153)->save($path1);

                    $path1 = $dirOptimised . $mediaFieName;
                    Image::make($post_media_link)->resize(600, null)->save($path1);

                    $post_media_link->move($dirOriginal, $mediaFieName);


                    $post->post_media_type = 'image';
                    $post->post_media_link = $mediaFieName;

                }

                $post->save();

                return redirect()->route('viewPosts');
            }else{
                return \Response::json(
                    [
                        'notification' => [
                            'type' => 'error',
                            'message' => "Format du fichier non supporté :( ". strtoupper($fileExtention)
                        ]
                    ],
                    400
                );
            }
        }else{
            $post->save();

            return redirect()->route('viewPosts');
        }
//        dd($post);
//        //return view('admin.post_detail')->with(['post' => $post]);
//        return redirect()->back();
    }

    public function editCategory(){
        $cat = Category::find(\Request::input('id'));
        $cat->categorie_name = \Request::input('categorie_name');
        $cat->save();
        return redirect()->route('viewCategories');
    }
}

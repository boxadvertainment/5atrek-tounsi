<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// Youtube Auth & Callback URLS (Do not edit or remove)
Route::get('/youtube-auth', 'AppController@youtubeAuth');
Route::get('/youtube-callback', 'AppController@youtubeAuth');


Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::auth();
    Route::get('/', 'AdminController@index');
    Route::get('posts/', 'AdminController@posts')->name('viewPosts');
    Route::get('participants/', 'AdminController@participants');
    Route::get('categories/', 'AdminController@categories')->name('viewCategories');
    Route::get('postByCategories/{id}', 'AdminController@postByCategories');
    Route::get('postAccept/{id}', 'AdminController@post_accept');
    Route::get('postRefused/{id}', 'AdminController@post_refused');

    Route::get('detail/{id}', 'AdminController@postDetail');
    Route::get('category-detail/{id}', 'AdminController@categoryDetail');

    Route::post('editPost/', 'AdminController@editPost');
    Route::post('editCategory/', 'AdminController@editCategory');
    Route::post('add-category/', 'AdminController@addCategory');

    Route::get('form-category/', 'AdminController@formCategory');

    Route::get('edit-participant/{id}', 'AdminController@editParticipant');
    Route::post('edit-participant/update/{id}', 'AdminController@updateParticipant');
});


Route::get('/{name?}' , 'AppController@index')->name('index');
Route::post('/insertion/' , 'AppController@postCreate');
Route::get('/action/{id}' , 'AppController@postDetail');
//Route::get('/liste-des-actions/{category}', 'AppController@postsList');

//Route::post('auth/facebookLogin/{offline?}', 'Auth\AuthController@facebookLogin');
//Route::post('signup', 'AppController@signup');


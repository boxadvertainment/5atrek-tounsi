<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'AppController@index')->name('index');
Route::get('form-post/', 'AppController@formPost');
Route::post('post-create/', 'AppController@postCreate');
Route::get('view-posts/{name}', 'AppController@viewPost');
// Route::post('auth/facebookLogin/{offline?}', 'Auth\AuthController@facebookLogin');
// Route::post('signup', 'AppController@signup');

// Youtube Auth & Callback URLS (Do not edit or remove)
Route::get('/youtube-auth', function () {
});
Route::get('/youtube-callback', function () {
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::auth();
    Route::get('/', 'AdminController@index');
    Route::get('posts/', 'AdminController@posts')->name('viewPosts');
    Route::get('participants/', 'AdminController@participants');
    Route::get('categories/', 'AdminController@categories')->name('viewCategories');
    Route::get('postByCategories/{id}', 'AdminController@postByCategories');

    Route::get('detail/{id}', 'AdminController@postDetail');
    Route::get('category-detail/{id}', 'AdminController@categoryDetail');

    Route::post('editPost/', 'AdminController@editPost');
    Route::post('editCategory/', 'AdminController@editCategory');
    Route::post('add-category/', 'AdminController@addCategory');

    Route::get('form-category/', 'AdminController@formCategory');
});

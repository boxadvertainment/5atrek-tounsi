@extends('layout')

@push('stylesheets')

@section('main_container')
        <div class="container-fuild">

            <div class="cta-mobile">
                <img src="{{ asset('images/logo-5atrek-tounsi.png') }}" class="main-logo" alt="">
                <p class="description">

                    خاطر التونسي هكة ، قلبو و روحو نبعت من هالتراب الغالي وخاطر حكاياتنا تنبع من قلبنا التونسي ...خلّيك تونسي و خلي خيرك سابق

                </p>
                <button class="btn fb-connect"> <i class="fa fa-check-circle"></i> Je participe</button>
            </div>
            <header class="listing-header">
                <h1 class="title clearfix"> <i class="fa fa-list"></i> {{ $page_title }} <br><small class="sub-info hidden-xs">{{ $actions_count }} personnes qui donnent espoir à notre Tunisie</small></h1>

                <select class="basic select-home pull-right" data-links="true"  data-placeholder-option="true" style="opacity: 0">
                    <option  value="">Toutes les catégories</option>
                    <option  value="@if($current_category  != 'all' ) {{ url('all') }} @endif" @if($current_category == 'all' ) selected @endif >Toutes les catégories</option>
                    @foreach($category_list as $item)
                        <option value="@if($current_category != $item->id ) {{ url($item->id) }} @endif" @if($current_category == $item->id ) selected @endif>{{ $item->categorie_name }}</option>
                    @endforeach
                </select>
            </header>

            <div class="row">

            </div>

        @if($list!=null)
            <div class="content-post">
                @include('posts')
            </div>
        @endif
        </div>

@endsection
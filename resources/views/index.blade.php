@extends('layout')

@push('stylesheets')

@section('main_container')

        <div class="container">
                <div class="col-sm-12">
                        <h1 class="center">Laravel Start !!!</h1>
                        <a href="{{ url('form-post') }}">Create post</a>
                        <ul>
                                <li><a href="{{ url('view-posts/all') }}">All</a></li>
                                @foreach($list as $item)
                                        <li><a href="{{ url('view-posts/'.$item->id) }}">{{ $item->categorie_name }}</a></li>
                                @endforeach
                        </ul>
                </div>
        </div>
@endsection
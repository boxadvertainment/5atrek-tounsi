@extends('admin.layouts.blank')

@push('stylesheets')

<!--   Exemple to push style -->
<!--<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">-->

@endpush

@section('main_container')

    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-10">
                <div class="form-detail">
                    <form class="form-horizontal" action="{{ url('admin/editPost') }}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <fieldset>
                            <legend class="text-center"><h1>Post details</h1></legend>

                            <!-- id input-->
                            <div class="form-group hide">
                                <label class="col-md-3 control-label" for="name">Id</label>
                                <div class="col-md-9">
                                    <input name="id" type="text" value="{{ $post->id }}" class="form-control">
                                </div>
                            </div>

                            <!-- Name participant input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name">Name participant</label>
                                <div class="col-md-9">
                                    <input name="name" type="text" value="{{ $post->participant->getFullName() }}" class="form-control" disabled>
                                </div>
                            </div>

                            <!-- post titre input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name">Post title</label>
                                <div class="col-md-9">
                                    <input name="post_titre" type="text" value="{{ $post->post_titre }}" class="form-control">
                                </div>
                            </div>

                            <!-- Image-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name">Média</label>
                                <div class="col-md-9">
                                    @if($post->post_media_type == "image")
                                        <img src="/app/images/original/{{ $post->getMessageImageThumb() }}" style="padding-bottom: 10px; width: 400px">
                                    @else
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$post->post_media_link}}" frameborder="0" allowfullscreen></iframe>
                                    @endif
                                    <input type="file" name="post_media_link" class="form-control">
                                </div>
                            </div>

                            <!-- Message body -->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="message">Your message</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="post_text" placeholder="Please enter your message here..." rows="5">{{ $post->post_text }}</textarea>
                                </div>
                            </div>

                            <!-- Date input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="email">Your date</label>
                                <div class="col-md-9">
                                    <input name="post_date" type="text" placeholder="Your date" class="form-control" value="{{ $post->post_date }}">
                                </div>
                            </div>

                            <!-- Region input-->
                            <div class="form-group">
                                <label class="control-label col-md-3">Region:</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="post_region">
                                        <option @if($post->post_region == "" ) selected @endif value="">Choix du région</option>
                                        <option @if($post->post_region == "Tunis" ) selected @endif value="Tunis">Tunis</option>
                                        <option @if($post->post_region == "Bizert" ) selected @endif value="Bizert">Bizert</option>
                                        <option @if($post->post_region == "Nabel" ) selected @endif value="Nabel">Nabel</option>
                                        <option @if($post->post_region == "Soussa" ) selected @endif value="Soussa">Soussa</option>
                                        <option @if($post->post_region == "Sfax" ) selected @endif value="Sfax">Sfax</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Catégorie-->
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Your Category</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select name="category_id" class="form-control">
                                        @foreach($category_list as $cat)
                                            <option value="{{ $cat->id }}" @if($post->category_id == $cat->id ) selected @endif>{{ $cat->categorie_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <!-- Status input-->
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Your status</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select name="post_status" class="form-control">
                                        <option @if($post->post_status == "pending") selected @endif value="Pending">Pending</option>
                                        <option @if($post->post_status == "accept") selected @endif value="Accepted">Accepted</option>
                                        <option @if($post->post_status == "refused") selected @endif value="Refused">Refused</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Status input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="email">Created </label>
                                <div class="col-md-9">
                                    <input type="text" placeholder="Your email" class="form-control" value="{{ $post->created_at }}" disabled>
                                </div>
                            </div>

                            <!-- Form actions -->
                            <div class="form-group">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                    <a href="javascript:history.go(-1)" class="btn btn-primary">Cancel</a>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript">
    $("#posts").attr("class", "active");
</script>

@endpush
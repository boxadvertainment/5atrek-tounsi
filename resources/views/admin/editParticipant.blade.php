@extends('admin.layouts.blank')

@push('stylesheets')

<!--   Exemple to push style -->
<!--<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">-->

@endpush

@section('main_container')

    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-10">
                <div class="form-detail">
                    <form class="form-horizontal" action="{{ action('Admin\AdminController@updateParticipant', ['id' => $participant->id]) }}" method="post">
                        {!! csrf_field() !!}
                        <fieldset>
                            <legend class="text-center"><h1>User details</h1></legend>

                            <!-- id input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="facebookID">Facebook ID</label>
                                <div class="col-md-9">
                                    <input name="facebook_id" id="facebookID" type="text" value="{{ $participant->facebook_id }}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name">Nom</label>
                                <div class="col-md-9">
                                    <input name="participant_nom" id="name" type="text" value="{{ $participant->participant_nom }}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="surname">Prénom</label>
                                <div class="col-md-9">
                                    <input name="participant_prenom" id="surname" type="text" value="{{ $participant->participant_prenom }}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="email">Email</label>
                                <div class="col-md-9">
                                    <input name="participant_email" id="email" type="email" value="{{ $participant->participant_email }}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
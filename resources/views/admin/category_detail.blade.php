@extends('admin.layouts.blank')

@push('stylesheets')

<!--   Exemple to push style -->
<!--<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">-->

@endpush

@section('main_container')

    <div class="right_col" role="main">
    <div class="row">
        <div class="col-md-10">
            <div class="form-detail">
                <form class="form-horizontal" action="{{ url('admin/editCategory') }}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <fieldset>
                        <legend class="text-center"><h1>Category details</h1></legend>

                        <!-- Name input-->
                        <div class="form-group hide">
                            <label class="col-md-3 control-label" for="name">Id</label>
                            <div class="col-md-9">
                                <input name="id" type="text" value="{{ $category->id }}" class="form-control">
                            </div>
                        </div>

                        <!-- Name input-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Name</label>
                            <div class="col-md-9">
                                <input name="categorie_name" type="text" value="{{ $category->categorie_name }}" class="form-control">
                            </div>
                        </div>

                        <!-- Status input-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Created </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="{{ $category->created_at }}" disabled>
                            </div>
                        </div>

                        <!-- Form actions -->
                        <div class="form-group">
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a href="javascript:history.go(-1)" class="btn btn-primary">Cancel</a>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $("#posts").attr("class", "active");
</script>

@endpush
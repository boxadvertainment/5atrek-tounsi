@extends('layout')

@section('main_container')
    <div class="col-sm-12">
        <h1 class="text-center">Poster votre message</h1>
        <form class="form-horizontal well col-md-12" method="post" action="{{ url('post-create') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="Titre">
                            Post title:
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" name="post_titre" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12" for="media">
                            Post media:
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="file" name="post_media_link" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Date of post</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input id="birthday" name="post_date" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Region:</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <select class="form-control" name="post_region">
                                <option value="">Choix du région</option>
                                <option value="Tunis">Tunis</option>
                                <option value="Bizert">Bizert</option>
                                <option value="Nabel">Nabel</option>
                                <option value="Soussa">Soussa</option>
                                <option value="Sfax">Sfax</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Categorie:</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <select class="form-control" name="category_id">
                                <option value="">Choix du catégorie</option>
                                @foreach($list as $cat)
                                    <option value="{{ $cat->id }}">{{ $cat->categorie_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Description:</label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <textarea class="form-control" rows="8" name="post_text"></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-9">
                            <button type="submit" class="btn btn-success">Envoyer</button>
                            <button type="reset" class="btn btn-primary">Cancel</button>
                            </div>
                        </div>
                </div>
            </div>
        </form>
    </div>

@endsection
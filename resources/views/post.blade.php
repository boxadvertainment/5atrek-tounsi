@extends('layout')

@section('main_container')
        <div class="col-sm-12">
            <h1 class="text-center">Poster votre message</h1>
            <form method="post" action="{{ url('post-create') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <label>Titre:</label>
                <input type="text" name="post_titre"><br>
                <label>Description:</label>
                <textarea name="post_text"></textarea><br>
                <label>Media:</label>
                <input type="file" name="post_media_link"><br>
                <label>Date:</label>
                <input type="datetime" name="post_date"><br>
                <label>Région:</label>
                <select name="post_region">
                    <option value="">Choix du région</option>
                    <option value="Tunis">Tunis</option>
                    <option value="Bizert">Bizert</option>
                    <option value="Nabel">Nabel</option>
                    <option value="Soussa">Soussa</option>
                    <option value="Sfax">Sfax</option>
                </select><br>
                <label>Catégorie</label>
                <select name="category_id">
                    <option value="">Choix du catégorie</option>
                    @foreach($list as $cat)
                        <option value="{{ $cat->id }}">{{ $cat->categorie_name }}</option>
                        @endforeach
                </select><br>
                <input type="submit" value="Envoyer">
            </form>
        </div>

@endsection
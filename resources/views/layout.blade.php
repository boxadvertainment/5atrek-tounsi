<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="fr"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title'){{ Config::get('app.name') }}</title>
    <meta name="author" content="box.agency" />

    @include('partials.socialMetaTags', [
        'title' => 'على خاطرك تونسي - Ala khatrek Tounsi',
        'description' => 'خاطر التونسي هكة ، قلبو و روحو نبعت من هالتراب الغالي وخاطر حكاياتنا تنبع من قلبنا التونسي ...خلّيك تونسي و خلي خيرك سابق'
    ])

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Use RealFaviconGenerator.net to generate favicons  -->
    @include('partials.favicon')

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Varela+Round" type='text/css'>
    <link rel="stylesheet" href="{{ asset('css/plugins-front.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    @stack('stylesheets')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ request()->url() }}';
        FB_APP_ID   = '{{ env('FACEBOOK_APP_ID') }}';
        APP_ENV     = '{{ app()->environment() }}';
    </script>
    <script src="{{ asset("js/modernizr.js") }}"></script>
</head>
<body class="@yield('class')">
    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>
    <![endif]-->

    <!--  Loader -->
    <div class="loader">
        <div class="spinner">
            <small>   Veuillez Patienter </small>
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!--  Loader -->
    <div class="progress-wrapper hide">
        <div class="container">
            <div class="col-md-6 col-md-offset-3 box">
                <h3 class="title">  Veuillez Patienter <br> <small>Vos données sont en cours d'envoi </small> </h3>
                <div class="progress">
                    <div class="progress-bar progress-bar-info active progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 7%;">
                        00%
                    </div>
                </div>
                <div class="stats-box">2Mo envoyés sur 10 Mo totale</div>
            </div>
        </div>
    </div>

    <!--  Loader -->
    <div class="loader">
        <div class="spinner">
            <small>   Veuillez Patienter </small>
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!-- App Wrapper -->
    <div id="wrapper" class="main_container">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <div class="sidebar-nav">
                <img src="{{ asset('images/logo-5atrek-tounsi.png') }}" class="main-logo" alt="Ala Khatrek Tounsi">
                <p class="description">
                    خاطر التونسي هكة ، قلبو و روحو نبعت من هالتراب الغالي وخاطر حكاياتنا تنبع من قلبنا التونسي ...خلّيك تونسي و خلي خيرك سابق
                </p>
                <button class="btn fb-connect"> <i class="fa fa-check-circle"></i> Je participe </button>

            </div>
            <div class="footer">
                <div class="top clearfix">
                    <div class="social-pages pull-left">
                        <p>Restez branchés <br>
                            <a href="http://facebook.com/Khatrektounsi" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/KhatrekTounsi" target="_blank"> <i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UC3W2ouuXa4PtJoB0OdppChg" target="_blank"> <i class="fa fa-youtube"></i></p></a>
                    </div>
                    <div class="share pull-left">
                        <p>Partagez notre plateforme <br>
                        <a href="#" class="fb-share"><i class="fa fa-facebook"></i> Partager </a></p>
                    </div>
                </div>
                <div class="copyright">
                    <p>Copyright © 2016 - Ala khatrek Tounsi</p>
                </div>
            </div>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            @yield('main_container')
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <div id="addPostModal" class="modal animated fadeInDown"  tabindex="-1" role="dialog" aria-labelledby="addPostModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body form">
                    <div class="profile-block text-center">
                        <div class="profile-picture"><img src="{{ asset('images/profile-pic-placeholder.png') }}" alt=""></div>
                        <div class="profile-info">
                            <h3 class="user-name">Foulen Ben Foulen</h3>
                            <p class="user-email">foulen@benfoulen.com</p>
                        </div>
                    </div>
                    <form class="clearfix" method="post" action="{{ url('insertion') }}" enctype="multipart/form-data" id="addPost">
                        <input type="hidden" name="user-first-name" class="user-first-name">
                        <input type="hidden" name="user-last-name" class="user-last-name">
                        <input type="hidden" name="user-email" class="user-email">
                        <input type="hidden" name="user-fb-id" class="user-fb-id">
                        <div class="col-sm-offset-2 col-sm-8">
                            <div class="form-group">
                                <label class="control-label">
                                    <span>Titre de votre action *:</span>
                                    <input type="text" name="post_titre" required="required" class="form-control" placeholder="Titre de votre action...">
                                </label>
                            </div>

                            <div class="form-group post-media">
                                <label class="control-label">
                                    <span>Image ou Vidéo de l'action *:</span>
                                    <div class="dropzone-wrapper" id="preview">
                                        <div class="file"><i class="fa fa-file"></i></div>
                                        <div class="file-name">Cliquer ou placer une image/vidéo ici</div>
                                        <div class="file-size"> On accepte que les image/vidéo (max : 20 Mo) </div>
                                        <input type="file" name="post_media_link" required="required" class="form-control dropzone-input">

                                    </div>

                                </label>
                            </div>

                            <div class="form-group">
                                <label class="control-label">
                                    <span>Date :</span>
                                    <input type="text" id="birthday" name="post_date" class="date-picker form-control" placeholder="Date de l'action">
                                </label>
                            </div>

                            <div class="form-group">
                                <label class="control-label">
                                    <span>Région *:</span>
                                    <select class="form-control" name="post_region" required>
                                        <option value="">Choisir une région</option>
                                        <option value="Ariana"> Ariana</option>
                                        <option value="Béja"> Béja</option>
                                        <option value="Ben Arous"> Ben Arous</option>
                                        <option value="Bizerte"> Bizerte</option>
                                        <option value="Gabès"> Gabès</option>
                                        <option value="Gafsa"> Gafsa</option>
                                        <option value="Jendouba"> Jendouba</option>
                                        <option value="Kairouan"> Kairouan</option>
                                        <option value="Kasserine"> Kasserine</option>
                                        <option value="Kébili"> Kébili</option>
                                        <option value="La Manouba"> La Manouba</option>
                                        <option value="Le Kef"> Le Kef</option>
                                        <option value="Mahdia"> Mahdia</option>
                                        <option value="Médenine"> Médenine</option>
                                        <option value="Monastir"> Monastir</option>
                                        <option value="Nabeul"> Nabeul</option>
                                        <option value="Sfax"> Sfax</option>
                                        <option value="Sidi Bouzid"> Sidi Bouzid</option>
                                        <option value="Siliana"> Siliana</option>
                                        <option value="Sousse"> Sousse</option>
                                        <option value="Tataouine"> Tataouine</option>
                                        <option value="Tozeur"> Tozeur</option>
                                        <option value="Tunis"> Tunis</option>
                                        <option value="Zaghouan"> Zaghouan</option>
                                        <option value="Autres"> Autres</option>
                                    </select>
                                </label>
                            </div>

                            <div class="form-group">
                                <label class="control-label">
                                    <span>Catégorie *:</span>
                                    <select class="form-control" name="category_id"  required>
                                        <option value="">Choisir une catégorie</option>
                                        @foreach($category_list as $cat)
                                            <option value="{{ $cat->id }}">{{ $cat->categorie_name }}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="control-label">
                                    <span>Description *:</span>
                                    <textarea class="form-control" rows="5" name="post_text" required></textarea>
                                </label>
                            </div>
                            {{ csrf_field() }}
                            <div class="form-group modal-btns clearfix">
                                <button type="submit" class="btn btn-success">Valider</button>
                                <button type="reset" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Annuler</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="actionDetail modal fade" tabindex="-1" role="dialog" id="actionDetail">
        <div class="modal-dialog">
            <div class="modal-content">
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="videoRecap">
        <div class="modal-dialog" style="width: 60%">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">&times;</button>
                {{--<div class="modal-header">
                    <h4 class="modal-title">Video récapitulatif</h4>
                </div>--}}
                <div class="modal-body">
                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/cbFs2yXwvTU" frameborder="0" allowfullscreen></iframe>
                    <!--<p>Context here</p>-->
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset("js/plugins-front.js") }}"></script>
    <script src="{{ asset("js/main.js") }}"></script>

    @stack('scripts')

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-82225961-1', 'auto');
        ga('send', 'pageview');
    </script>

<script>
    $(document).ready(function () {

        $('#videoRecap').modal('show');

    });

</script>

</body>
</html>
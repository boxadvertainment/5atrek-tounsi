var elixir = require('laravel-elixir');
require('./elixir-extensions');

elixir(function (mix) {
    mix
        .sass('main.scss', 'public/css/main.css')
        .sass('admin.scss', 'public/css/admin.css')
        //.babel(['main.js'], 'public/js/main.js')
        //.babel('admin.js', 'public/js/admin.js')
        //.babel('admin.js',{srcDir:'resources/assets/js',output:'public/js/admin.js',sourceMap:true})
        .scripts([
            // bower:js
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
            'bower_components/parsleyjs/dist/parsley.js',
            'bower_components/Chart.js/dist/Chart.js'
            // endbower

        ], 'public/js/main.js', 'resources/assets/js')
        .styles([
            // bower:css
            'bower_components/animate.css/animate.css'
            // endbower
        ], 'public/css/vendor.css', 'bower_components')
        .copy('bower_components/font-awesome/fonts', 'public/fonts')
        //.version(['css/main.css', 'js/main.js'])
        //.copy('public/fonts', 'public/build/fonts')
        .images()
        .wiredep()
        .minifyCss()
        .compress()
        .browserSync({
            proxy: process.env.APP_URL
        });
});

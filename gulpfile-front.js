var elixir = require('laravel-elixir');
require('./elixir-extensions');

elixir(function(mix) {

    mix
        .sass('main.scss')
        .babel([
            'facebookUtils.js',
            'main.js'
        ], 'public/js/main.js')

        /*******************     scripts front End start here          ******************/
        .scripts([
            // bower:js
            'bower_components/jquery/dist/jquery.js',
            'bower_components/SelectOrDie/_src/selectordie.min.js',
            'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
            'bower_components/sweetalert2/dist/sweetalert2.js',
            'bower_components/datedropper/datedropper.min.js',
            // endbower
        ], 'public/js/plugins-front.js', 'bower_components')
        /*******************     styles front End start here          ******************/
        .styles([
            // bower:css
            'bower_components/animate.css/animate.css',
            'bower_components/SelectOrDie/_src/selectordie.css',
            'bower_components/font-awesome/css/font-awesome.css',
            'bower_components/sweetAlert2/dist/sweetalert2.css',
            'bower_components/datedropper/datedropper.min.css',
            // endbower
        ], 'public/css/plugins-front.css', 'bower_components')


});
